#### Background Information
##### Motivation
Our primary objective is to generate robot-centric conceptual knowledge about objects from measurements of properties of objects estimated from the sensory data. In order to achieve this goal, we propose a multi-layered knowledge acquisition system that can be used to generate robot-centric conceptual knowledge about household objects, where each layer is built upon a layer below by abstracting over the lower layer, consequently denoting the different levels of abstraction at each layer. According to our proposal, acquisition of the robot-centric conceptual knowledge in a bottom-up fashion should take place in two steps: First, we capture the sensory data about various physical properties of objects. The sensory data is then processed to estimate quantitative measurements of physical and functional properties observed in the objects which is then used to generate the desired knowledge about objects. The figure below illustrates
such a multi-layered system wherein the bottom three layers form the proposed sensor data extraction of the properties of objects followed by property estimations. The top two layers form the proposed generation of robot-centric conceptual knowledge about objects which is covered in the git repository
<mark>[Knowledge Generation](https://gitlab.com/rock_paper_scissors/knowledge-generation)</mark> project.

![Property Estimation](https://drive.google.com/uc?export=view&id=1QabNEK46Pz81U1lDUdN3tlTLikMVXKIm 'Property Estimation Layers')

We propose a property estimation framework called Robot-Centric Dataset (RoCS) framework that contains multiple property estimation methods which can be used to estimate the measurements of various physical and functional properties of objects. The primary objective behind the proposed framework is to build an extensible system such that it encases various property estimation methods that can estimate the measurement of a property from a single object instance in real-time. In order to attain that we have proposed expert-defined estimation methods which estimate a property measurement in a single object instance which are discussed in detail in our journal article[^1]. The figure below illustrates the modular structure of the RoCS framework and the resulting measurement data to be supplied to the Knowledge Generation system. 

![Estimation Framework](https://drive.google.com/uc?export=view&id=1ziO02MsghxL18G4pLFbzYHb7DtDkZCvi 'RoCS property estimation framework for extracting sensory data')

It primarily consists of two modules: *Online Data Acquisition* and *Property Estimation*. The Online Data Acquisition module is responsible for capturing the raw sensory data using different sensors from a single object instance in real-time. The sensory data is supplied to the Property Estimation module which consists of three phases as depicted in the figure above. In the *Feature Extraction* phase, the desired features are extracted from the sensory data. These features form the basis for estimating a property measurement in an object instance. The extracted features are integrated in the *Feature Integration* phase to form the primary parameters required for estimating the measurements of the properties. The last phase consists of computing the quantitative measurements using the proposed expert-based estimation methods. The quantitative measurements of the properties are then forwarded to the *Knowledge Generation* system for generating conceptual knowledge about objects.  
In the figure below, we have illustrates an implementation of each of the modules to generate quantitative measurements of six physical and four functional properties where functional properties are measured in terms the physical properties that enable them.

![Property Measurements](https://drive.google.com/uc?export=view&id=1vVeNYQ8sN63p_P3XB86_AdJCRLtQCSQR 'Features used to measure properties')

In the figure, we have captured the sensory data from an RGB-D sensor, a robotic arm and a household scale. The *Feature Extraction* phase contains six features extracted from the RGB-D sensor data, three features from a robotic arm and a single feature from a scale. Note that the selection of sensors and the subsequent features to be extracted from the sensory data depend on how a property is computationally defined for estimating the measurements. In the figure, the *Feature Integration* phase illustrates which features are integrated for each property estimation. The last phase consists of computing the quantitative measurements.

[^1]: see our journal article detailing the methods: Thosar, M., Mueller, C. A., Jaeger, G., Schleiss, J., Pulugu, N., Chennaboina, R. M., … Zug, S. (2021). From Multi-modal Property Dataset to Robot-Centric Conceptual Knowledge About Household Objects. Frontiers in Robotics and AI - Computational Intelligence in Robotics, Special Issue: Robots That Learn and Reason: Towards Learning Logic Rules from Noisy Data, 8, 87.

# Usage
## Cloning the Project
#### One-Line-Solution:
```
bash -c 'eval $(ssh-agent) && ssh-add && git clone --recurse-submodules git@gitlab.com:RocSet/rocs.git'
```
#### Explanation:
You want to clone a specific branch
```
git clone --recurse-submodules git@gitlab.com:RocSet/rocs.git
# optional: -b branch    to recursively clone a specific branch
```

To avoid having to unlock your rsa-key for each of the recursive clones,
you might want to run an ssh-agent.
For example you can run `ssh-add`. 
If this results into 
`'Could not open a connection to your authentication agent.'`, you need to start
the ssh-agent with `eval $(ssh-agent)`. 
Then you can run `ssh-add` again and after that the `git clone` command above.

Everything together:
```
eval $(ssh-agent)
ssh-add
git clone --recurse-submodules git@gitlab.com:RocSet/rocs.git
```


Alternatively you can run
```
git clone git@gitlab.com:RocSet/rocs.git
# You now have just the folder structure.
# You can remove folders of package-submodules you don't need.

# Then you can pull all submodules recursively:
git submodule update --init --recursive
# or go into each package folder and run 'git pull' manually.

# If you need to pull the real latest versions of all remote gitmodules
#  and not the latest versions stored as links in this repository, 
#  you might want to run
git submodule update --recursive --remote

# If you want to checkout a specific branch (e.g., master, develop-integrate) of tthe submodules
git submodule foreach -q --recursive 'git checkout $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo develop-integrate)'

# If you want to display the status of submodules when git status is executed:
git config --global status.submoduleSummary true
```

### Editing
If you wan't to edit Files in the submodules, go into the folder of the submodule.
```
# for example
cd rocs/src/rocs_property_aquisition
```

Remember that this subfolder is a git-repository itself.
This git repository is pointing to a specific commit, which might be outdated.
So you might need to force a checkout of your branch, for example:
```
git checkout -f origin develop-integrate
```

Now you can work in the submodule.
Make sure, you are in the subfolder before committing or pushing.

## Maintenace 
To keep the submodules updated, you need to update them from time to time:
```
git submodule update --recursive --remote
```
If there were changes, `git commit --all -m 'updated submodules'` and `git push`.

## Building and Running
You can either build and run inside of a 
[Docker](https://www.docker.com/)-Container
following the instructions in 
[Building the Docker Container](#building-the-docker-container)
and 
[Running the Docker container](#running-the-docker-container)
or you can build and run on your host machine after installing 
[RoS](http://www.ros.org/)
following the instructions in
[Compiling and Running without Docker](#compiling-and-running-without-docker).

## Building the Docker Container
Then you can build the Docker-Image:
```
cd rocs
docker build -t rocs:kinetic-test .
```

## Running the Docker container
### Check
Make shure you have the docker image in your local docker repository:
```
docker images
```
This should list the repository rocs and a tag kinetic-test.

### Create and run Container
Now you can create a container from the image:
```
docker run -dit --name RoCS -v $(pwd):/RoCS:rw rocs:kinetic-test

## Important docker run options:
# -it   makes the container start with a open bash shell
# -d    deamonizes this shell and therefore make the container run infinitely (so it doesn't stop immediately)
# -v $(pwd):/RoCS:rw   mounts the current directory as /RoCS folder in the container
#                      This is usefull, if you've built the docker image yourselve from this git repository
#                      By adding this option you replace the resources which were copied into the image with
#                       a link to the sources on your host machine.
#                      Now you can edit the sources in your host system in an IDE like you are used to and
#                       compile, test and run them in the docker container without the need to copy them into
#                       container or re-build the whole image.
#                      You might want to omit this parameter if you use a pre-build docker image or just don't 
#                       plan on modifying the sources.
# optional: -p 80:80   assigns the port 80 of the docker container to port 80 of the host machine
```
The Container should now run in the backround of your machine.

### Bash in Container 
To get access to to the Container run 
```
docker exec -it RoCS bash
```
which will give you access to a bash inside of the docker container.

You might need to run 
```
source /opt/ros/kinetic/setup.bash 
```
which will make catkin and rosbuild available on bash.


## Compiling and Running without Docker
Follow the official guide to set up RoS kinetic.
It's recommended to do this on Ubuntu Xenial.
See: https://wiki.ros.org/kinetic/Installation/Ubuntu

You might additionally need to install the packages listed in the Dockerfile:
```
sudo apt-get update 

# make shure the base- and perception-packages are installed:
sudo apt-get install \
        ros-kinetic-ros-core \
        ros-kinetic-ros-base \
        ros-kinetic-perception

# install additional dependencies:
sudo apt-get install \
	ros-kinetic-aruco \
	ros-kinetic-brics-actuator \
	ros-kinetic-pr2-msgs \
	ros-kinetic-joy \
	ros-kinetic-tf-conversions \
	ros-kinetic-control-msgs \
	libgsl-dev \
	libcgal-qt5-11 \
	libcgal-dev \
```

## Possible Problems
### Docker not installed
If you run into problems:
Make shure you have docker installed:
```
sudo apt-get install docker
```

### Docker Deamon not running
Make shure the docker deamon is running:
```
systemctl status docker
```
If the deamon is not running, start it with
```
systemctl start docker
```
If you want to start the docker daemon on every boot (might be a additional havy load and not recommended on systems relying on battery) run
```
systemctl enable docker
```

### Building fails because of `sudo` in `youbot_driver`
If you get errors while running or building the docker container because of `sudo`, make shure that `rocs_yourbot_arm/RoCS/devel/lib/youbot_driver_ros_interface/youbot_driver_ros_interface` line 41 is set to `OFF` like
```
option(USE_SETCAP "Set permissions to access ethernet interface without sudo" OFF)
```

### Invalid cross-device link
If you get the following error while building or running the container:
```
dpkg: error: error creating new backup file '/var/lib/dpkg/status-old': Invalid cross-device link
```
you have metacopy enabled in your kernel, which is default in Linux 4.19 Kernel.
Run 
```
echo N | sudo tee /sys/module/overlay/parameters/metacopy
```
on your host machine.
See [https://github.com/docker/for-linux/issues/480].

# Info
Follow the following conventions for storing the raw data(rosbags) obtained from different property extraction methods:

	- Path to store rosbags - rocs_property_data/object_data/<propertyName>_data/
	- Naming convention of rosbags - <propertyName>_experiment_<objectName>_<dateTime>


WEIGHT DATA still in question?

Launch the file 'bringup_ROSBag_property_acquisition.launch' to launch the pipeline for property extraction

The file bringup_ROSBag_property_acquisition.launch will in turn launch the following nodes/launch files

	- The node 'rocs_rosbag_loader.py' in 'rocs_rosbag_loader' package
		- The script will collect and merge raw data of different properties and publishes the merged data
		- Every repetition of each instance is published as a message of type 'Object' on '/object' topic

	- The file 'run_property_acquisition.launch' in 'rocs_meta' package
		- The launch file in turn launches the property_aquisition nodes of each property
		- The property_aquisition nodes are subscribed to '/object' topic
		- The property_aquisition nodes calculate the property values from the raw data and publish the results on message type 'Property' with topic name '/object_property'

	- The file 'run_property_knowledge_base.launch' in 'rocs_property_knowledge_base' package
		- The launch file launches the node 'property_knowledge_base_execution_node'
		- The node is subcribed to topic '/object_property'
		- The node saves the property values published on '/object_property' into '.yml' files. The files are saved under the folder 'rocs_property_data'/property_data'

Once all the '.yml' files are generated run the 'wrapper.py' script from the terminal to generate the csv file of each property_type

	- The 'wrapper.py' script is under the path 'rocs_property_data/scripts'

# Packages:
	* aruco
		- visual detection of markers
	* cloud_registration
		- pointcloud something with mages
	* data_loader
		- probably deprecated
	* experiment_control
		- main component for running hardware experiments
		- each experiment has a set of launch scripts and configurations
	* experiment_setup
		- first implementation of visual experiment
		- possibly deprecated
	* marker_detection
		- based on aruco mapping
	* meta
		- configuration and launch files
		- maybe merge with experiment_control
	* msgs
		- message types are defined here
	* object_segmenter
		- extracts object from pointcloud
	* point_cloud_aggregation
		- ???
	* property_acquisition
		- property extraction methods
		- physical and functional properties
		- maybe split?
	* property_data
		- gathered and extracted data will be saved here
		- relative location
	* property_knowledge_base
		- is this in use?
		- should be used for data transformation
	* rosbag_inspector
		- measuring efforts for rigidity experiment
		- only needed for development to define thresholds
		- maybe better solution? moveit?
	* rosbag_loader
		- not implemented or old
		- replay data gathered by rosbag_inspector
	* scripts
		- includes some jupyter script with data
	* sensors
		- contains calibration data for asus xtion (not axtion) camera
		- this is probably not in use ???
	* youbot_arm
		- driver for kuka youbot
		- includes gamepad control
