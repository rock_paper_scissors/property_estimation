FROM ros:kinetic-perception
RUN apt-get update \
    && apt-get install -y \
	ros-kinetic-aruco \
	ros-kinetic-brics-actuator \
	ros-kinetic-pr2-msgs \
	ros-kinetic-joy \
	ros-kinetic-tf-conversions \
	ros-kinetic-control-msgs \
	libgsl-dev \
	libcgal-qt5-11 \
	libcgal-dev \
    && rm -rf /var/lib/apt/lists/*

# following packages will allso be installed automatically by the command above:
#   libgmp-dev
#   libgmpxx4ldbl
#   libmpfr-dev
#   libcgal11v5


COPY src /RoCS/src
# or to download code from url:
# ADD url /RoCS/

WORKDIR /RoCS/
RUN /bin/bash -c "source /opt/ros/kinetic/setup.bash && catkin_make"
                    

# TODO:
# CMD [ "roslaunch", "my-ros-app my-ros-app.launch" ]

# NOTES:
#
# Make shure that rocs_yourbot_arm/RoCS/devel/lib/youbot_driver_ros_interface/youbot_driver_ros_interface line 41 is set to OFF like
# option(USE_SETCAP "Set permissions to access ethernet interface without sudo" OFF)
#
# If you get the following error:
#  dpkg: error: error creating new backup file '/var/lib/dpkg/status-old': Invalid cross-device link
# you have metacopy enabled in your kernel, which is default in Linux 4.19 Kernel
# Run 
#  echo N | sudo tee /sys/module/overlay/parameters/metacopy
# on your host machine.
